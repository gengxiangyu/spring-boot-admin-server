package com.admin.server;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableAdminServer // 启用 Spring Boot Admin 服务端
@SpringBootApplication
public class AdminServerApplication implements WebMvcConfigurer {

    private final static Logger logger = LoggerFactory.getLogger(AdminServerApplication.class);

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(AdminServerApplication.class, args);
        ConfigurableEnvironment environment = context.getEnvironment();
        // 端口号
        String serverPort = environment.getProperty("server.port");
        // 访问前缀
        String contextPath = environment.getProperty("server.servlet.context-path");
        logger.info("Spring-Boot-Admin 监控服务启动成功，访问地址为：http://localhost:" + serverPort + contextPath);
    }

    /** 静态资源处理器 */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // 注册静态资源：设置项目的静态资源访问
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
    }
}
